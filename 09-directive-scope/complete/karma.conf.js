var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.plugins.push('karma-ng-html2js-preprocessor');

  config.files.push('09-directive-scope/complete/app.js');
  config.files.push('09-directive-scope/complete/service.js');
  config.files.push('09-directive-scope/complete/*.spec.js');
  config.files.push('09-directive-scope/complete/*.html');

  if (!config.preprocessors['**/*.html']) {
    config.preprocessors['**/*.html'] = [];
  }

  config.preprocessors['**/*.html'].push('ng-html2js');

  config.ngHtml2JsPreprocessor = {
    stripPrefix: '09-directive-scope/complete/',
    moduleName: 'bacon.templates'
  }
};
