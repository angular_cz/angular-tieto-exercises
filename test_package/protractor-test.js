'use strict';

describe('test-package', function() {

  it('should contain headline on index page', function() {
    browser.get('index.html')
        .then(function() {
          expect(
            element(by.css('h2')).getText()
              ).toMatch('Vítejte na školení AngularJS');
        });
  });


});
