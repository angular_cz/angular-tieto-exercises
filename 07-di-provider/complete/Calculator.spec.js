describe('calculator', function() {

  beforeEach(function() {
    module('diApp');
    angular.module('diApp')
      .config(function(calculatorProvider) {
        calculatorProvider.setPagePrice(3);
      });
  });

  it('should return 90 when format is A5 and number of pages is 0', inject(function(calculator) {

      var product = {
        pageSize: 'A5',
        numberOfPages: 0
      };

      expect(calculator.getPrice(product)).toEqual(90);
    })
  );

  it('should return 120 when format is A5 and number of pages is 50', inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    expect(calculator.getPrice(product)).toEqual(120);
  }));

});