describe('bacon-ipsum component', function() {
  beforeEach(module('bacon', 'bacon.templates'));

  beforeEach(inject(function($rootScope, $compile, generator) {
    this.generator = generator;

    spyOn(generator, 'getParagraphs').and.callFake(
      function(numberOfParagraphs) {
        numberOfParagraphs = numberOfParagraphs || 1;

        var paragraphs = ['DATA_FROM_GENERATOR', 'SECOND_LINE', 'THIRD_LINE'];
        return paragraphs.slice(0, numberOfParagraphs);
      });

    this.$compile = $compile;
    this.$rootScope = $rootScope;

    this.$scope = this.$rootScope.$new();

    this.setTemplate = function(template) {
      this.element = angular.element(template);
    };

    this.render = function() {
      var element = this.$compile(this.element)(this.$scope);
      this.$scope.$digest();

      return element;
    };

    this.setTemplate('<bacon-ipsum></bacon-ipsum>');
  }));

  it('renders template (TODO 1.1.1)', function() {
    expect(this.render().html()).toMatch('<cite>Bacon ipsum</cite>');
  });

  it('adds data to the template (TODO 1.1.2)', function() {
    var template = this.render();
    var controller = template.isolateScope().$ctrl || template.isolateScope().bacon;

    expect(controller.data).not.toBeFalsy();
  });

  it('has controller named bacon (TODO 1.1.2)', function() {
    var template = this.render();
    expect(template.isolateScope().bacon).not.toBeFalsy();
  });

  it('renders paragraphs (TODO 1.1.2)', function() {
    expect(this.render().find('p').length).not.toBeFalsy();
  });

  it('uses generator service, (TODO 1.2)', function() {
    this.render();
    expect(this.generator.getParagraphs).toHaveBeenCalled();
  });

  it('renders paragraphs from generator (TODO 1.2)', function() {
    expect(this.render().find('p').html()).toMatch('DATA_FROM_GENERATOR');
  });

  describe('with multiple paragraphs', function() {

    beforeEach(function() {
      this.setTemplate('<bacon-ipsum paragraphs="2"></bacon-ipsum>');
    });

    it('get number of paragraphs from binding (TODO 2.1)', function() {
      var template = this.render();
      expect(template.isolateScope().bacon.paragraphs).toBe('2');
    });

    it('calls generator service with number of paragraphs (TODO 2.1)', function() {
      this.render();
      expect(this.generator.getParagraphs).toHaveBeenCalledWith('2');
    });

    it('renders number of paragraphs from generator (TODO 2.1)', function() {
      expect(this.render().find('p').length).toBe(2);
    });
  });

  describe('with defined title', function() {

    beforeEach(function() {
      this.setTemplate('<bacon-ipsum title="titleVariable"></bacon-ipsum>');
    });

    it('has initial value from expression (TODO 2.2)', function(){
      this.$scope.titleVariable = 'TITLE_VARIABLE';
      expect(this.render().html()).toMatch('TITLE_VARIABLE');
    });

    it('can have value changed throught binding (TODO 2.2)', function(){
      var template = this.render();

      this.$scope.titleVariable = 'CHANGED_VARIABLE';
      this.$scope.$digest();

      expect(template.html()).toMatch('CHANGED_VARIABLE');
    });
  });

});
